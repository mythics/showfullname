﻿# ShowFullName

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_6_20140226
* License: MIT

## Overview
This component overrides dynamichtml includes to use dFullName instead of dUserName wherever possible.

* Dynamichtml Includes:
	- std_mailto_user_address_link: Core - override to change mailToUser variable value from dUserName to dFullName
	- author_href_address: Core - override to use std_mailto_user_address_link if not a webdav request; needed to prevent inteference from DesktopIntegrationSuite
	- checkout_author_href_address: Core - override to use std_mailto_user_address_link if not a webdav request; needed to prevent inteference from DesktopIntegrationSuite
	- showfullname_rowdata_alter_user_fields: ShowFullName - change user fields to use mailto link and dFullName
	- showfullname_output_email_and_name: ShowFullName - display mailto link and dFullName for a specified dUserName
	- showfullname_output_user_list: ShowFullName - output mailto link and dFullName from list of dUserNames in userNameStr
	- create_slim_table_row_include: Core - override to add showfullname_rowdata_alter_user_fields include
	- workflow_info: ShowFullName - copied from Core WF_INFO template; called from WF_INFO template; override to change dUserName(s) to use mailto link and dFullName
	- author_checkin_field: ShowFullName - override to allow drop down list of dDocAuthors on search pages; AllowQuerySafeUserColumns must be true of dDocAuthors

* Templates:
	- WORKFLOW_INFO: Core - override to use workflow_info dynamichtml include

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 10.1.3.5.1 (111229) (Build:7.2.4.105)
* 11.1.1.8.1DEV-2014-01-06 04:18:30Z-r114490 (Build:7.3.5.185)

## Changelog
* build_6_20140226
	- Added code to display dFullName for dCheckoutUser
* build_5_20131106
	- Fixed issue with folder dDocAuthor
* build_4_20131105
	- Fixed sorting on step users to sort of dFullName instead of dUserName
	- Display mailto link and dFullName for sorted list of remaining approvers
	- Removed preference prompt to enable/disable since this is difficult to utilize with templates
	- Allow dDocAuthor list for search pages
* build_3_20131101
	- Added Workflow Info override
* build_2_20131031
	- Added Search Results override
	- Added FrameworkFolders browsing override
* build_1_20131030
	- Initial component release